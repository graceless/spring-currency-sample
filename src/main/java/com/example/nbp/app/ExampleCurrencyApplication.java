package com.example.nbp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class ExampleCurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleCurrencyApplication.class, args);
	}
}
