package com.example.nbp.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties("true")
public class Gold {

    private String data;
    private float cena;

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = data;
    }

    public float getCena() {
        return cena;
    }

    public void setCena(final float cena) {
        this.cena = cena;
    }

    public Gold(final String data, final float cena) {
        this.data = data;
        this.cena = cena;
    }

    public Gold() {
    }

    @Override
    public String toString() {
        return "Gold{" + "Data=" + data + ", cena=" + cena + '}';
    }
}
