package com.example.nbp.app.model;

public class Rates {

    private String no;
    private String effectiveDate;
    private String mid;
    private String ask;

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "Rates{" + "no='" + no + '\'' + ", effectiveDate='" + effectiveDate + '\'' + ", mid='" + mid + '\'' +
                ", ask='" + ask + '\'' + '}';
    }
}





