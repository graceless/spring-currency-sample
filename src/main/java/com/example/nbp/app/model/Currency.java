package com.example.nbp.app.model;

import java.lang.reflect.Array;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {

    public Currency() {
    }

    private String table;
    private int mid;
    private int ask;
    private String currency;
    private String effectiveDate;
    private Rates[] rates;

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Rates[] getRates() {
        return rates;
    }

    public void setRates(Rates[] rates) {
        this.rates = rates;
    }

    public int getAsk() {
        return ask;
    }

    public void setAsk(int ask) {
        this.ask = ask;
    }

    public Currency(int mid, String currency, String effectiveDate, Array[] rates, int ask) {
        this.mid = mid;
        this.currency = currency;
        this.effectiveDate = effectiveDate;
        this.rates = new Rates[1];
        this.ask = ask;
    }

    @Override
    public String toString() {
        return "Currency{" + "mid=" + mid + ", ask=" + ask + ", currency='" + currency + '\'' + ", effectiveDate='" +
                effectiveDate + '\'' + ", rates=" + Arrays.toString(rates) + '}';
    }
}



