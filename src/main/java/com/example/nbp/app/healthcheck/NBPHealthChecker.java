package com.example.nbp.app.healthcheck;

import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.nbp.app.service.CurrencyService;
import com.example.nbp.app.service.GoldService;

@Service
public class NBPHealthChecker {
    private final static Logger LOGGER = Logger.getLogger(NBPHealthChecker.class.getName());

    private final CurrencyService currencyService;

    private final GoldService goldService;

    public NBPHealthChecker(final CurrencyService currencyService, final GoldService goldService) {
        this.currencyService = currencyService;
        this.goldService = goldService;
    }

    @Scheduled(fixedRate = 1000)
    public void checkHealth() {
        try {
            LOGGER.info("Creating call to all of the services to check their health");
            currencyService.get("USD");
            goldService.get();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
