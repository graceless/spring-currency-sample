package com.example.nbp.app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.nbp.app.model.Currency;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class CurrencyService {

    @Value("${currency.url}")
    private String url;
    private final RestTemplate rest = new RestTemplate();
    private final static ObjectMapper mapper = new ObjectMapper();


    public ResponseEntity<String> get(String currency) {
        if (currency == null || currency == "") {
            throw new IllegalArgumentException();
        }

        HttpEntity<String> requestEntity = new HttpEntity<>("");
        ResponseEntity<String> responseEntity = rest.exchange(url + currency + "/?format=json",
                HttpMethod.GET,
                requestEntity,
                String.class);

        return responseEntity;
    }

    public Currency createCurrencyObject(ResponseEntity<String> entity) throws JsonProcessingException {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        return mapper.readValue(entity.getBody(), Currency.class);
    }
}