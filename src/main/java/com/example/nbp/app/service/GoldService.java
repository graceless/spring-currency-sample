package com.example.nbp.app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.nbp.app.model.Gold;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GoldService {

    @Value("${gold.url}")
    private String url;
    private final RestTemplate rest = new RestTemplate();
    private final static ObjectMapper mapper = new ObjectMapper();

    public ResponseEntity<String> get() {
        HttpEntity<String> requestEntity = new HttpEntity<>("");
        ResponseEntity<String> responseEntity = rest.exchange(url + "/?format=json",
                HttpMethod.GET,
                requestEntity,
                String.class);

        if (responseEntity == null) {
            throw new IllegalArgumentException();
        }
        return responseEntity;
    }

    public Gold createGoldObject(ResponseEntity<String> entity) throws JsonProcessingException {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        Gold[] asArray = mapper.readValue(entity.getBody(), Gold[].class);
        return asArray[0];
    }
}