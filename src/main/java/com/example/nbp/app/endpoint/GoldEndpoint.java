package com.example.nbp.app.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.nbp.app.model.Gold;
import com.example.nbp.app.service.GoldService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@RestController
public class GoldEndpoint {

    @Autowired
    GoldService goldService;

    public GoldEndpoint(final GoldService goldService) {
        this.goldService = goldService;
    }

    @ApiOperation("get gold value for the day")
    @GetMapping(value = "/gold")
    @ApiResponse(code = 200, message = "gold value for today retrieved successfully", response = Gold.class)

    public Gold goldValueRetriever() throws JsonProcessingException {
        if (goldService.get() == null) {
            throw new NullPointerException();
        }
        return goldService.createGoldObject(goldService.get());
    }
}


