package com.example.nbp.app.endpoint;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.nbp.app.common.CurrencyConstants;
import com.example.nbp.app.model.Currency;
import com.example.nbp.app.service.CurrencyService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
public class CurrencyEndpoint {

    @Autowired
    CurrencyService currencyService;

    public CurrencyEndpoint(final CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @ApiOperation("get mid value for chosen currency")

    @ApiResponses(value = {@ApiResponse(code = SC_OK, message = "ok"),
            @ApiResponse(code = SC_BAD_REQUEST, message = "An unexpected error occurred")})

    @GetMapping(value = "/mid")
    @ApiResponse(code = 200,
            message = "mid value for chosen currency retrieved successfully",
            response = Currency.class)
    public Currency midValueRetriever(
            @RequestParam(required = false, name = "currency")
                    String currency) throws JsonProcessingException {

        ResponseEntity<String> response = Arrays.stream(CurrencyConstants.values())
                .map(CurrencyConstants::name)
                .filter(commonConstants -> commonConstants.equalsIgnoreCase(currency))
                .findAny()
                .
                        map(currencyService::get)
                .orElseThrow(() -> new RuntimeException("Invalid symbol of the currency"));

        return currencyService.createCurrencyObject(response);
    }

}
