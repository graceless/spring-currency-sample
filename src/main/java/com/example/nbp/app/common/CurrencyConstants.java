package com.example.nbp.app.common;

public enum CurrencyConstants {
    USD, PLN, CHF, AFN, EUR, ALL, AUD, RON, GBP
}
