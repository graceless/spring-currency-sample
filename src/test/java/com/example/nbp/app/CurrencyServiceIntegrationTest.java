package com.example.nbp.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.logging.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.nbp.app.service.CurrencyService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CurrencyServiceIntegrationTest {
    private final static Logger LOGGER = Logger.getLogger(CurrencyServiceIntegrationTest.class.getName());
    @Autowired
    private CurrencyService currencyService;

    @Test
    void emptyResponseEntityWillThrowIAE() {
        LOGGER.info("Checking if empty Response entity will throw Illegal Argument Exception.");
        ResponseEntity responseEntity = null;

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(currencyService.createCurrencyObject(responseEntity));
        });
        LOGGER.info("Empty Response entity will throw Illegal Argument Exception.");
    }

    @Test
    void whenCurrencyValueIsInvalidCurrencyServiceWillThrowRTE() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            currencyService.get("One");
        });
    }

    @Test
    void whenCurrencyValueIsNullCurrencyServiceWillThrowIAE() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            currencyService.get(null);
        });
    }

    @Test
    void whenCurrencyIsCorrectThen200IsReceived() {
        ResponseEntity<String> response = currencyService.get("usd");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}