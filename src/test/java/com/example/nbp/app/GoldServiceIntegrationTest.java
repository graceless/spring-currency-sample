package com.example.nbp.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.nbp.app.service.GoldService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class
GoldServiceIntegrationTest {

    @Autowired
    private GoldService goldService;

    @Test
    void emptyResponseEntityWillThrowIAEWhenCreateGoldObject() {
        ResponseEntity responseEntity = null;

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(goldService.createGoldObject(responseEntity));
        });
    }

    @Test
    void whenCurrencyIsCorrectThen200IsReceived() {
        ResponseEntity<String> response = goldService.get();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}