package com.example.nbp.app;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.nbp.app.endpoint.CurrencyEndpoint;
import com.example.nbp.app.model.Currency;
import com.example.nbp.app.service.CurrencyService;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CurrencyServiceUnitTest {

    @Test
    void currencyServiceCreatesObjectWithAccurateMid() throws JsonProcessingException {
        CurrencyService mock = mock(CurrencyService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("\n" + "{\"mid\":5,\"ask\":0," +
                "\"currency\":\"dolar amerykański\",\"effectiveDate\":null," +
                        "\"rates\":[{\"no\":\"099/A/NBP/2020\",\"effectiveDate\":\"2020-05-22\",\"mid\":\"4.1503\"," +
                        "\"ask\":null}]}",
                header,
                HttpStatus.OK);
        when(mock.get("USD")).thenReturn(responseEntity);
        when(mock.createCurrencyObject(responseEntity)).thenCallRealMethod();
        CurrencyEndpoint currencyEndpoint = new CurrencyEndpoint(mock);
        Currency getCurrency = currencyEndpoint.midValueRetriever("USD");
        Assertions.assertEquals(getCurrency.getMid(), 5);
    }

    @Test
    void currencyServiceCreatesObjectWithAccurateCurrency() throws JsonProcessingException {
        CurrencyService mock = mock(CurrencyService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("\n" + "{\"mid\":5,\"ask\":0," +
                "\"currency\":\"dolar amerykański\",\"effectiveDate\":null," +
                        "\"rates\":[{\"no\":\"099/A/NBP/2020\",\"effectiveDate\":\"2020-05-22\",\"mid\":\"4.1503\"," +
                        "\"ask\":null}]}",
                header,
                HttpStatus.OK);
        when(mock.get("USD")).thenReturn(responseEntity);
        when(mock.createCurrencyObject(responseEntity)).thenCallRealMethod();
        CurrencyEndpoint currencyEndpoint = new CurrencyEndpoint(mock);
        Currency getCurrency = currencyEndpoint.midValueRetriever("USD");
        Assertions.assertEquals(getCurrency.getCurrency(), "dolar amerykański");
    }

    @Test
    void CSCreatesCurrencyObjectWithNullValuesIfResponsebodyIsEmpty() throws JsonProcessingException {
        CurrencyService mock = mock(CurrencyService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("{}]",
                header,
                HttpStatus.OK);
        when(mock.get("USD")).thenReturn(responseEntity);
        when(mock.createCurrencyObject(responseEntity)).thenCallRealMethod();
        CurrencyEndpoint currencyEndpoint = new CurrencyEndpoint(mock);
        Currency getCurrency = currencyEndpoint.midValueRetriever("USD");
        Assertions.assertEquals(getCurrency.getCurrency(), null);
    }
}

