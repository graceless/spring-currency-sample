package com.example.nbp.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.example.nbp.app.endpoint.GoldEndpoint;
import com.example.nbp.app.model.Gold;
import com.example.nbp.app.service.GoldService;
import com.fasterxml.jackson.core.JsonProcessingException;

class GoldServiceUnitTest extends Mockito {

    @Test
    void goldServiceCreatesObjectWithAccurateCena() throws JsonProcessingException {
        GoldService mock = mock(GoldService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("[{\"data\":\"2020-05-20\",\"cena\":232}]",
                header,
                HttpStatus.OK);
        when(mock.get()).thenReturn(responseEntity);
        when(mock.createGoldObject(responseEntity)).thenCallRealMethod();
        GoldEndpoint goldEndpoint = new GoldEndpoint(mock);
        Gold getGold = goldEndpoint.goldValueRetriever();
        Assertions.assertEquals(getGold.getCena(), 232);
    }

    @Test
    void goldServiceCreatesObjectWithAccurateData() throws JsonProcessingException {
        GoldService mock = mock(GoldService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("[{\"data\":\"2020-05-20\",\"cena\":232}]",
                header,
                HttpStatus.OK);
        when(mock.get()).thenReturn(responseEntity);
        when(mock.createGoldObject(responseEntity)).thenCallRealMethod();
        GoldEndpoint goldEndpoint = new GoldEndpoint(mock);
        Gold getGold = goldEndpoint.goldValueRetriever();
        Assertions.assertEquals(getGold.getData(), "2020-05-20");
    }

    @Test
    void GSCreatesGoldObjectWithNullValuesIfResponsebodyIsEmpty() throws JsonProcessingException {
        GoldService mock = mock(GoldService.class);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("[{}]", header, HttpStatus.OK);
        when(mock.get()).thenReturn(responseEntity);
        when(mock.createGoldObject(responseEntity)).thenCallRealMethod();
        GoldEndpoint goldEndpoint = new GoldEndpoint(mock);
        Gold getGold = goldEndpoint.goldValueRetriever();
        String sampleData = null;
        Assertions.assertEquals(getGold.getData(), sampleData);
    }
}
